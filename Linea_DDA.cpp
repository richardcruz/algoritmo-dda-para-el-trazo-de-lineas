#include <GL/glut.h>
#include <windows.h>
#include <math.h>
#include <iostream>

using namespace std;

void iniciarGLUT(){

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(600, 600);
	glutCreateWindow("Linea DDA");
}

void proyectar(){

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0.0, 200.0, 0.0, 200.0);
	glClear(GL_COLOR_BUFFER_BIT);
}

void setPixel(int x, int y) {

	glColor3f(0.0, 0.0, 0.0);
	glPointSize(3);
	glBegin(GL_POINTS);
        glVertex2f(x, y);
	glEnd();
	glFlush();
}

void lineDDA(float xi, float yi, float xf, float yf){

    float dx, dy, steps, k;
    float xIncrement, yIncrement, x, y;

    dx=xf-xi;
    dy=yf-yi;

    if(abs(dx)>abs(dy))
        steps=abs(dx);
    else
        steps=abs(dy);

    xIncrement=dx/steps;
    yIncrement=dy/steps;
    x=xi;
    y=yi;

    setPixel(round(x), round(y));
    cout<<"("<<x<<", "<<y<<")"<<endl;

    for(k=1; k<=steps; k++)
    {
        x=x+xIncrement;
        y=y+yIncrement;
        setPixel(round(x), round(y));
        cout<<"("<<x<<", "<<y<<")"<<endl;
    }
}

int main (int argc, char** argv) {

	float xi, yi, xf, yf;

	cout<<"\t\tALGORITMO DDA GENERAL PARA RECTAS\n";

	cout<<"\n> Ingrese puntos: \n\n";
	cout<<"  Coordenada xi: "; cin>>xi;
	cout<<"  Coordenada yi: "; cin>>yi;
	cout<<"\n  Coordenada xf: "; cin>>xf;
	cout<<"  Coordenada yf: "; cin>>yf;

	glutInit(&argc, argv);
	iniciarGLUT();
	proyectar();

	lineDDA(xi, yi, xf, yf);

	glutMainLoop();

	return 0;
}

